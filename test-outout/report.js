$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Order.feature");
formatter.feature({
  "line": 2,
  "name": "OrderGuestPart1",
  "description": "",
  "id": "orderguestpart1",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@OrderGuest"
    }
  ]
});
formatter.before({
  "duration": 4054779800,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "user access to website",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "User opens the Website",
  "keyword": "Given "
});
formatter.match({
  "location": "Step_SignIn.user_opens_the_Website()"
});
formatter.result({
  "duration": 2572856800,
  "status": "passed"
});
formatter.scenario({
  "line": 199,
  "name": "TC010 - Guest User - Verify User is able to track a Delivery Order just after Ordering",
  "description": "",
  "id": "orderguestpart1;tc010---guest-user---verify-user-is-able-to-track-a-delivery-order-just-after-ordering",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 198,
      "name": "@test010"
    }
  ]
});
formatter.step({
  "line": 200,
  "name": "user clicks on Order button",
  "keyword": "Given "
});
formatter.step({
  "line": 201,
  "name": "user enters the address",
  "rows": [
    {
      "cells": [
        "address",
        "1130 Universiy Blvd, Tuscaloosa, AL"
      ],
      "line": 202
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 203,
  "name": "user clicks on Delivery button",
  "keyword": "And "
});
formatter.step({
  "line": 204,
  "name": "user select Date and Time from Calendar",
  "keyword": "And "
});
formatter.step({
  "line": 205,
  "name": "user clicks on Continue",
  "keyword": "And "
});
formatter.step({
  "line": 206,
  "name": "On Menu Page click on The Insomniac",
  "keyword": "And "
});
formatter.step({
  "line": 207,
  "name": "user clicks on add product",
  "keyword": "And "
});
formatter.step({
  "line": 208,
  "name": "user goes to the cart",
  "keyword": "And "
});
formatter.step({
  "line": 209,
  "name": "user clicks on the checkout button",
  "keyword": "And "
});
formatter.step({
  "line": 210,
  "name": "user enters Valid Details under Delivery Info \u0026 your Info",
  "rows": [
    {
      "cells": [
        "recepient name",
        "Theresa Ainsworth"
      ],
      "line": 211
    },
    {
      "cells": [
        "recepient phone",
        "2345678981"
      ],
      "line": 212
    },
    {
      "cells": [
        "customer name",
        "Prateek Nehra"
      ],
      "line": 213
    },
    {
      "cells": [
        "customer phone",
        "3457689024"
      ],
      "line": 214
    },
    {
      "cells": [
        "customer emailID",
        "icprateeknehra@gmail.com"
      ],
      "line": 215
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 216,
  "name": "user enter delivery message",
  "rows": [
    {
      "cells": [
        "instructions",
        "deliver fresh warm cookies"
      ],
      "line": 217
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 218,
  "name": "user selects Payment Method as Credit Card",
  "keyword": "And "
});
formatter.step({
  "line": 219,
  "name": "user enters credit card details",
  "rows": [
    {
      "cells": [
        "cc no",
        "4111111111111111"
      ],
      "line": 220
    },
    {
      "cells": [
        "cc expiry",
        "03 20"
      ],
      "line": 221
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 222,
  "name": "user clicks on Place Order",
  "keyword": "And "
});
formatter.step({
  "line": 223,
  "name": "user clicks on Track Order button",
  "keyword": "And "
});
formatter.step({
  "line": 224,
  "name": "Tracking ID should be displayed along with status",
  "keyword": "Then "
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Order_button()"
});
formatter.result({
  "duration": 13282634100,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_enters_the_address(DataTable)"
});
formatter.result({
  "duration": 14487814500,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Delivery_button()"
});
formatter.result({
  "duration": 10688443300,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_select_Date_and_Time_from_Calendar()"
});
formatter.result({
  "duration": 3304567300,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Continue()"
});
formatter.result({
  "duration": 10292567900,
  "status": "passed"
});
formatter.match({
  "location": "Step_Order.on_Menu_Page_click_on_the_Insomniac()"
});
formatter.result({
  "duration": 51171261500,
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for element to be clickable: Proxy element for: DefaultElementLocator \u0027By.xpath: //div[@id\u003d\u0027deals\u0027]//div[5]//div[1]//div[1]//img[1]\u0027 (tried for 30 second(s) with 500 MILLISECONDS interval)\r\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:80)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:232)\r\n\tat com.qa.stepDefinition.Step_Order.on_Menu_Page_click_on_the_Insomniac(Step_Order.java:786)\r\n\tat ✽.And On Menu Page click on The Insomniac(Order.feature:206)\r\nCaused by: org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//div[@id\u003d\u0027deals\u0027]//div[5]//div[1]//div[1]//img[1]\"}\n  (Session info: chrome\u003d87.0.4280.141)\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.5.3\u0027, revision: \u0027a88d25fe6b\u0027, time: \u00272017-08-29T12:42:44.417Z\u0027\nSystem info: host: \u0027EC2AMAZ-445LITV\u0027, ip: \u0027172.31.18.183\u0027, os.name: \u0027Windows Server 2019\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_261\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d87.0.4280.88 (89e2380a3e36c3464b5dd1302349b1382549290d-refs/branch-heads/4280@{#1761}), userDataDir\u003dC:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\2\\scoped_dir6716_2095360422}, timeouts\u003d{implicit\u003d0, pageLoad\u003d300000, script\u003d30000}, pageLoadStrategy\u003dnormal, unhandledPromptBehavior\u003ddismiss and notify, strictFileInteractability\u003dfalse, platform\u003dXP, proxy\u003dProxy(), goog:chromeOptions\u003d{debuggerAddress\u003dlocalhost:57197}, acceptInsecureCerts\u003dfalse, browserVersion\u003d87.0.4280.141, browserName\u003dchrome, javascriptEnabled\u003dtrue, platformName\u003dXP, setWindowRect\u003dtrue, webauthn:virtualAuthenticators\u003dtrue}]\nSession ID: 7cf083413263fe80709d037dd176f427\n*** Element info: {Using\u003dxpath, value\u003d//div[@id\u003d\u0027deals\u0027]//div[5]//div[1]//div[1]//img[1]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:185)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:120)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:82)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:646)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:416)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:518)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:361)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:408)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\r\n\tat com.sun.proxy.$Proxy20.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:302)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:44)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:288)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:285)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:673)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$23.apply(ExpectedConditions.java:669)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:209)\r\n\tat com.qa.stepDefinition.Step_Order.on_Menu_Page_click_on_the_Insomniac(Step_Order.java:786)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\r\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\r\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\r\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\r\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.internal.runners.statements.RunAfters.evaluate(RunAfters.java:27)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:89)\r\n\tat org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:41)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:542)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:770)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:464)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:210)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_add_product()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.user_goes_to_the_cart()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_the_checkout_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.user_enters_Valid_Details_under_Delivery_Info_your_Info(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.user_enter_delivery_message(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.user_selects_Payment_Method_as_Credit_Card()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.user_enters_credit_card_details(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.user_clicks_on_Place_Order()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.track_order()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "Step_Order.verify_tracking_ID()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1419075900,
  "status": "passed"
});
});