package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.utils.TestBase;

public class HomePage extends TestBase{
	
	public HomePage(WebDriver driver) {
		// super(driver);
		PageFactory.initElements(driver, this);

	}
	
	
	@FindBy(xpath = "//button[@id='order-now-btn']")
	public WebElement homePage_OrderNowBtn;
	
	@FindBy(xpath = "//a[@class='navbar-brand navbar-brand-desktop']//img")
	public WebElement IC_Logo;
	
	@FindBy(xpath = "//input[@id='email-register']")
	public WebElement email;
	
	@FindBy(xpath = "//input[@id='password-register']")
	public WebElement password;
	
	@FindBy(xpath = "//input[@id='password-confirm']")
	public WebElement passwordConfirm;
	
	
	
	
	
	

}
