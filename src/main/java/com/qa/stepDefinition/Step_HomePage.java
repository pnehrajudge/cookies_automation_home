package com.qa.stepDefinition;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.HomePage;
import com.qa.pages.Order;
import com.qa.utils.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class Step_HomePage extends TestBase {
	
	WebDriver driver = getDriver();
	 WebDriverWait wait = new WebDriverWait(driver, 12);
	 
	 HomePage homePage = new HomePage(driver);
	
	@Given("^user Enters the website url$")
	 public void user_enters_the_Website_url() throws Throwable {				   		

	}
	
	@Then("^Verify Insomnia Cookies Homepage is displayed$")
	 public void verify_home_page_isDisplayed() throws Throwable {				   		
		wait.until(ExpectedConditions.visibilityOf(homePage.homePage_OrderNowBtn));
		if(homePage.homePage_OrderNowBtn.isDisplayed()) {
			System.out.println("Home pge open Success");
		}else {
			Assert.fail("Home Page didn't open");
		}
	}
	
	@Then("^Verify the IC logo in the left side of header$")
	public void verify_IC_Logo() {
		wait.until(ExpectedConditions.visibilityOf(homePage.IC_Logo));
		if(homePage.homePage_OrderNowBtn.isDisplayed()) {
			System.out.println("Home page open Success");
		}else {
			Assert.fail("Home Page didn't open");
		}
	}

}
