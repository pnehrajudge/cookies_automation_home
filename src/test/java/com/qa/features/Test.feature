@TestOrder
Feature: Order_Refactoring

  Background: user access to ADMIN site
  Given user opens the ADMIN site

  @adminTest  
  Scenario: TC 025 CC Order placed then Card Processor changed to PNP and a New CC order placed for Store Bryn Mawr
  Given verify if user logged in
  When user clicks stores
  And user search for store
  |Bryn Mawr|
  And user click payments
  Then verify by default chase is selected as CC processor else update to chase
  |chase| 
    Given User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | Bryn Mawr, North Bryn Mawr Avenue, Bryn Mawr, PA 19010 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    When user opens the ADMIN site
    And verify if user logged in
    And user clicks stores
    And user search for store
    |Bryn Mawr|
    And user click payments
    And user change Credit Card Processor to PNP
    |pnp|
    And user clicks Admin Update button
    Then Verify credit card Processor succesfully changed
    Then User opens the Website
    And if user is already logged in- just logout
    Given user clicks on Order button
    When user enters the address
      | address | Bryn Mawr, North Bryn Mawr Avenue, Bryn Mawr, PA 19010 |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
      |cvv|111|
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
