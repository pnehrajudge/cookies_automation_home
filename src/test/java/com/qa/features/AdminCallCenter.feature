@callcenterTest
Feature: CallCenter to place order for the customer

  Background: user access to ADMIN site
    Given user opens the ADMIN site

  Scenario: TC 001 Verify user is succesfully able to navigate to Admin > Call Center module
    Given Verify if user is logged in
    When user clicks CallCenter
    Then Verify call center module is open succesfully

  @CallCenterDebug
  Scenario: TC 002 Verify clicking Clear all button clears the form on Customer tab
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    When User Clicks Clear all button
    Then Verify the customer form gets cleared

  @DevCallCenter
  Scenario: TC 018 Verify clicking Clear all button clears the form on Shipping tab
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    When User Clicks Clear all button on shipping tab
    Then Verify the Shipping form gets cleared

  Scenario: TC 049 Verify clicking Cancel Order on Order tab cancels the order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    And Click Cancel Order button
    Then verify the order gets cancelled

  @DevCallCenter
  Scenario: TC 008 Enter an existing cutomer name and verify customer details gets auto populated under Select Customer drop down list box
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And user waits for the list to populate
    And User enters LastName
      | Nehra |
    Then verify the existing customer information gets displayed in Select Customer drop down list

  Scenario: TC 009 Verify the admin is able to place a Pickup order for a new customer
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  @DevCallCenter
  Scenario: TC 038 Verify the admin is able to Edit a Pickup Order from Billing Tab and removes an item from the order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user Adds another Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user clicks on Edit Order button
    And user removes an item from cart
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  @DevCallCenter
  Scenario: TC 023 Verify the admin is able to Edit a Pickup Order from billing Tab and adds an item to the order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user clicks on Edit Order button
    And user Adds another Item
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC  Verify the admin is able to Edit a Pickup Order from billing Tab and change the Order date and Time
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user clicks on Edit Order button
    And user edits delivery date as today
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 043 Verify the admin is able to place a Delivery order for a new customer
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters random FirstName
      | Test |
    And User enters LastName
      | Name |
    And user enters PhoneNumber
      | 6745989457 |
    And user enters email
      | test@test123.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 024 Verify the admin is able to place a Delivery order for an Off Campus Location
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 025 Verify the admin is able to place a Delivery order for an On Campus Location
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select On Campus
    And user select campus building
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC  Verify the admin is able to Edit a Delivery Order from Billing Tab and removes an item from the order and then place the order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user Adds another Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user clicks on Edit Order button
    And user removes an item from cart
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 038 Verify the admin is able to Edit a Delivery Order from billing Tab and adds  an item to the order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user clicks on Edit Order button
    And user Adds another Item
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  @DevCallCenter
  Scenario: TC  Verify the admin is able to Edit a Delivery Order from billing Tab and change the Order Date and Time
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user clicks on Edit Order button
    And user edits delivery date as today
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 044 Verify admin is able to place an order using Credit Card as Payment method
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Credit Card as payment method
    And user enters CC details
      | cc no | 4111111111111111 |
      | cvv   |              111 |
    And Click Complete order for CC payment
    Then Verify Congrats message for successful Order Process

  @DevCallCenter
  Scenario: TC 045 Verify admin is able to place an order using Cash on Dleivery as Payment method
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  @DevCallCenter
  Scenario: TC 046 Verify admin is able to place an order using Gift Card as Payment method
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Gift Card as payment method
    And user Enters Gift card details
      | Gift Card No | 9999998888877652 |
    And Click Complete order for gift card
    Then Verify Congrats message for successful Order Process

  @DevCallCenter
  Scenario: TC 057 Verify admin is able to place a Pickup order for an existing customer
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And user waits for the list to populate
    And User enters LastName
      | Nehra |
    And User Selects the existing customer from the customer list
    Then Verify Shipping tab is open
    And user select saved Address
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 047 Verify admin is able to place a Delivery order for an existing customer
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And user waits for the list to populate
    And User enters LastName
      | Nehra |
    And User Selects the existing customer from the customer list
    Then Verify Shipping tab is open
    And user select saved Address
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC  Verify after placing an order on clicking Back To Call center button, user navigates to the  Customer Information form
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process
    When User Clicks Back To Call Center button
    Then Verify call center module is open succesfully

  Scenario: TC 022 Verify after placing an order on clicking Start New Order button, user navigates to the Customer Information form
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process
    When User Clicks Back To Call Center button
    Then Verify call center module is open succesfully

  @DevCallCenter
  Scenario: TC 048 Verify admin is able to place a Delivery order using School cash as Nova Cash
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 1084 lancaster avenue bryn mawr |
    And user click Delivery Button
    And user select On Campus
    And user select campus building
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    When user selects Nova Cash as payment method
    And user enters School Cash
      | 6010000000000000 |
    And Click Complete order for School Cash
    Then Verify Congrats message for successful Order Process

  @DevCallCenter
  Scenario: TC  Verify Cookie Cake order requires 1 hr lead time
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds Cookie Cake to cart
    And user selects delivery date as today
    And user select the delivery time as store open time
    And User Clicks the checkout Button
    Then verify validation message stating Cake orders take longer to bake. is displayed

  @DevCallCenter
  Scenario: TC 026 Verify that Cookie Cake can't be placed for pickup until 1 hr after the store opens.
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds Cookie Cake to cart
    And user selects delivery date as today
    And user select the delivery time as store open time
    And User Clicks the checkout Button
    Then verify validation message stating Cake orders take longer to bake. is displayed

  Scenario: TC 026 Verify Admin is able to add products in cart successfully
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9876543210 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open

  Scenario: TC 022 Verify Admin is able to select Delivery method for the address
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters random FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9876543210 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open

  Scenario: TC 027 Verify Admin is able to select products by clicking on Assortment button and add to cart
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9876543210 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Clicks on Assortment
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open

  Scenario: TC 053  Verify admin is able to place pickup order using Credit Card as Payment method
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9876543210 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Credit Card as payment method
    And user enters CC details
      | cc no | 4111111111111111 |
      | cvv   |              111 |
    And Click Complete order for CC payment
    Then Verify Congrats message for successful Order Process

  Scenario: TC 048 Verify admin is able to place a Pickup order using School cash as Nova Cash
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 1084 lancaster avenue bryn mawr |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    When user selects Nova Cash as payment method
    And user enters School Cash
      | 6010000000000000 |
    And Click Complete order for School Cash
    Then Verify Congrats message for successful Order Process

  Scenario: TC 062 Verify when admin Selects Payment method than "Complete order" button is displayed
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 058 Verify admin is able to checkout from order Page and moves to billing page by clicking on Checkout button
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open

  Scenario: TC 041 Verify Admin is able to update the delivery date and time in cart
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And user selects delivery date as tomorrow on billing page
    And user select the delivery time
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 054  Verify admin is able to place Pickup order using Cash on Delivery as Payment method
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process

  Scenario: TC 055 Verify admin is able to place Pickup order using Gift Card as Payment method
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Gift Card as payment method
    And user Enters Gift card details
      | Gift Card No | 9999998888877651 |
    And Click Complete order for gift card
    Then Verify Congrats message for successful Order Process

  Scenario: TC 011 Verify When Admin enters new customer details and clicks on Continue button than moves to next page
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9876543210 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open

  Scenario: TC 031 - Verify Admin is able to apply Amount coupon for Delivery Order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And admin enters Coupon
      | Amount code | TestCoupon |
    And user clicks Apply button
    And Amount coupon should be added successfully to the cart admin

  Scenario: TC 037 Verify Admin is able to Add Upsell to the Delivery orders
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user adds upsell
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order is displayed

  Scenario: TC 063 Verify After Placing order admin gets message for successfully placed order on Order Complete Page
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user adds upsell
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order is displayed

  Scenario: TC 032 - Verify Admin is able to apply Percentage coupon for Delivery Order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And admin enters Coupon
      | Amount code | warmgifts |
    And user clicks Apply button
    And Percentage coupon should be added successfully to the cart admin

  Scenario: TC 033 - Verify Admin is able to apply Product coupon for Delivery Order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And admin enters Coupon
      | Amount code | 6 free cookies |
    And user clicks Apply button
    And user enters product quantity
    And user clicks add free items button
    And free product should be added successfully to the cart

  Scenario: TC 034 Verify Admin is able to apply Delivery coupon for Delivery Order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Vijay |
    And User enters LastName
      | Lachwani |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | v@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And admin enters Coupon
      | Amount code | TestFreeDelivery |
    And user clicks Apply button
    And free shipping should be added successfully to the cart

  Scenario: TC 020 Verify Admin is able to select address from the address drop down
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    Then Verify Delivery button appears on screen

  Scenario: TC 019 Verify Admin gets address suggestion while entering address in address box
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the initial words of address
      | 100 |
    Then verify address suggestion appears

  Scenario: TC 021	Verify when Admin enters Address than Map and Address is present with Pickup and Delivery button on right side
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    Then Verify Delivery button appears on screen
    And Verify Pickup button appears on screen
    And Map should be displayed

  Scenario: TC 064 Verify on Clicking on "Start new order" button on Order complete page takes admin to customer page
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Credit Card as payment method
    And user enters CC details
      | cc no | 4111111111111111 |
      | cvv   |              111 |
    And Click Complete order for CC payment
    Then Verify Congrats message for successful Order Process
    And Press Start new order button
    Then verify customer tab is open

  Scenario: TC 065 Verify on Clicking on Back to Call Center button on Order complete page takes admin to customer page
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Credit Card as payment method
    And user enters CC details
      | cc no | 4111111111111111 |
      | cvv   |              111 |
    And Click Complete order for CC payment
    Then Verify Congrats message for successful Order Process
    And Press Back to Call Center Button
    Then verify customer tab is open

  Scenario: TC 015 Verify Admin gets validation message  Please enter Email Address for customer for Email Address field
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user clicks on Continue Button
    Then verify enter email address pop appears

  Scenario: TC 012 Verify Admin gets validation message Please enter first name for customer for First Name field
    Given Verify if user is logged in
    When user clicks CallCenter
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then verify enter first name pop appears

  Scenario: TC 013 Verify Admin gets validation message Please enter Last name for customer for Last Name field
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then verify enter last name pop appears

  Scenario: TC 014 Verify Admin gets validation message Please enter Phone Number for customer for Phone number field
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then verify enter phone number pop appears

  Scenario: TC 016 Verify Admin gets validation messge Please enter a valid email address for customer for invalid email address
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test |
    And user clicks on Continue Button
    Then verify enter valid email address pop appears

  Scenario: TC 010 Verify when Admin select Customer from auto populated box and clicks on "Select Customer" button than Admin is redirected to next page
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And user waits for the list to populate
    And User enters LastName
      | Nehra |
    Then verify the existing customer information gets displayed in Select Customer drop down list
    And Click on Existing customer name
    And Click on Select Customer Button
    Then Verify Shipping tab is open

  Scenario: TC 017 Veirfy the customer info gets auto filled when Admin visit next page "Shipping"
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And user waits for the list to populate
    And User enters LastName
      | Nehra |
    Then verify the existing customer information gets displayed in Select Customer drop down list
    And Click on Existing customer name
    And Click on Select Customer Button
    Then Verify Shipping tab is open
    And Verify First name textbox in filled

  Scenario: TC 059 Verify by clicking on Edit order button on billing page, admin moves back to order page
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select On Campus
    And user select campus building
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And click edit order button
    Then verify Order Tab is open

  Scenario: TC 005 Verify when Admin visits call center page than by default "Yes" is selected for "Join Email List?" option
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    Then verify join email checkbox is checked

  Scenario: TC 007 Veirfy Admin is able to select "No" for "Join Email List?" option
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    Then verify join email checkbox is checked
    And click on No for Join email List checkbox
    Then verify no is selected for join email list

  Scenario: TC 004 Verify when Admin visits call center page than by default "Yes" is selected for "Create Account?" option and "Create New Account" option is present for Account
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    Then verify create account checkbox is checked

  Scenario: TC 050 Verify Admin moves to previous page by clicking on "Go Back " Button on every page present
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters random FirstName
      | Test |
    And User enters LastName
      | Name |
    And user enters PhoneNumber
      | 6745989457 |
    And user enters email
      | test@test123.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And click go back button billing page
    Then verify Order Tab is open
    And click go back button order page
    Then Verify Shipping tab is open
    And click go back button shipping page
    Then verify customer tab is open

  Scenario: TC 040 Verfiy Admin is able to place ASAP order for delivery
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select On Campus
    And user select campus building
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    And default selected time is asap
    And User Clicks the checkout Button
    Then Verify Billing tab is open

  Scenario: TC 051 Verfiy Admin is not able to place ASAP order for Pickup order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click PickUp Button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as tomorrow
    Then verify asap option should not be displayed

  Scenario: TC 039 Verify Admin is able to Delete the Product from the cart and again adds the product and places order
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters random FirstName
      | Test |
    And User enters LastName
      | Name |
    And user enters PhoneNumber
      | 6745989457 |
    And user enters email
      | test@test123.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    Then user deletes the items in cart
    And user again Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Credit Card as payment method
    And user enters CC details
      | cc no | 4111111111111111 |
      | cvv   |              111 |
    And Click Complete order for CC payment
    Then Verify Congrats message for successful Order Process

  Scenario: TC 066 Verify for Pickup order Admin is able to update phone number on billing page
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters random FirstName
      | Test |
    And User enters LastName
      | Name |
    And user enters PhoneNumber
      | 6745989457 |
    And user enters email
      | test@test123.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    Then user deletes the items in cart
    And user again Adds an Item
    And user selects delivery date as tomorrow
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open

  @NewAdminDebug
  Scenario: TC 036 Verify that order can be placed 6 months in advance
    Given Verify if user is logged in
    When user clicks CallCenter
    And user enters FirstName
      | Prateek |
    And User enters LastName
      | Nehra |
    And user enters PhoneNumber
      | 9687946385 |
    And user enters email
      | test@test.com |
    And user clicks on Continue Button
    Then Verify Shipping tab is open
    When user Enters the address and click Find Stores
      | 87 Mill Street, Starkville, MS 39759, USA |
    And user click Delivery Button
    And user select Off Campus
    And click Continue Delivery button
    Then verify Order Tab is open
    When user Adds an Item
    And user selects delivery date as 6 months from today
    And user select the delivery time
    And User Clicks the checkout Button
    Then Verify Billing tab is open
    And user selects Cash on Delivery as payment method
    And Click Complete order
    Then Verify Congrats message for successful Order Process
