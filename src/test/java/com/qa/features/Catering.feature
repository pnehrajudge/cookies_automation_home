@catering
Feature: Catering

Background: user access to website
Given User opens the Website without clearing cache

@DevCatering
Scenario: User place a Delivery Catering Order for 50Cookies
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 50Cookies
    And user clicks pick for me    
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
Scenario: User place a Delivery Catering Order for 100Cookies
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 100Cookies
    And user clicks pick for me    
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
     
Scenario: User place a Delivery Catering Order for 200Cookies
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 200Cookies
    And user clicks pick for me    
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
   
Scenario: User place a Delivery Catering Order for 300Cookies
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 300Cookies
    And user clicks pick for me    
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

@DevCatering
Scenario: User place a Delivery Catering Order for Big Birthday Celebration #Bug ID: 6085
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on The Big Birthday Celebration
    And user clicks pick for me    
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

 
Scenario: User place a Delivery Catering Order for Birthday Celebration
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Birthday Celebration
    And user clicks pick for me     
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary


Scenario: User place a Delivery Catering Order for Crowd Pleaser  #Bug ID: 6085
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Crowd Pleaser
    And user clicks pick for me   
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

Scenario: User place a Delivery Catering Order for Ice Cream Bar
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Ice Cream Bar
    And user clicks pick for me   
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
@DevCatering     
Scenario: User place a Delivery Catering Order for Office Party
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Office Party
    And user clicks pick for me   
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
Scenario: User place a Delivery Catering Order for Party Starter
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Party Starter
    And user clicks pick for me   
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
  
Scenario: User place a Delivery Catering Order for Sweet-Tooth Sampler
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Sweet-Tooth Sampler
    And user clicks pick for me   
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
@DevCatering 
Scenario: Verify a catering delivery order can be placed between 10AM to 6PM only
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time outside 10AM to 6PM from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Ice Cream Bar
    And user clicks pick for me   
    And user goes to the cart
    And user clicks on the checkout button
    Then verify validation message pops up to select time between 10 Am to 6PM
 
# need to write the script for this scenarios once the functionality is in place 
Scenario: Verify a catering order must be placed 2 hours in advance

@DevCatering
Scenario: Verify user is able to place a Delivery catering order for multiple menu items
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Delivery button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 100Cookies
    And user clicks pick for me
    And On Menu Page click on Birthday Celebration
    And user clicks pick for me
    And On Menu Page click on Ice Cream Bar
    And user clicks pick for me
    And On Menu Page click on Party Starter
    And user clicks pick for me    
    And On Menu Page click on Sweet-Tooth Sampler
    And user clicks pick for me   
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
 
Scenario: Verify user is able to place a Pickup catering order for multiple menu items
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 100Cookies
    And user clicks pick for me
    And On Menu Page click on Birthday Celebration
    And user clicks pick for me
    And On Menu Page click on Ice Cream Bar
    And user clicks pick for me
    And On Menu Page click on Party Starter
    And user clicks pick for me    
    And On Menu Page click on Sweet-Tooth Sampler
    And user clicks pick for me   
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
   
Scenario: User place a Pickup Catering Order for 50Cookies
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 50Cookies
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
   
Scenario: User place a Pickup Catering Order for 100Cookies
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 100Cookies
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
      
Scenario: User place a Pickup Catering Order for 200Cookies
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 100Cookies
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

@DevCatering     
Scenario: User place a Pickup Catering Order for 300Cookies
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on 100Cookies
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary

     
Scenario: User place a Pickup Catering Order for Big Birthday Celebration  #Bug ID: 6085
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on The Big Birthday Celebration
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    

Scenario: User place a Pickup Catering Order for Birthday Celebration
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Birthday Celebration
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
   
Scenario: User place a Pickup Catering Order for Crowd Pleaser  #Bug ID: 6085
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Crowd Pleaser
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    

Scenario: User place a Pickup Catering Order for Ice Cream Bar
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Ice Cream Bar
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
Scenario: User place a Pickup Catering Order for Office Party
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Office Party
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
   
Scenario: User place a Pickup Catering Order for Sweet-Tooth Sampler
Given User clicks on Cater
And User clicks on catering
When user enters the address
      | address | 140 16th St, phila pa |
    And user clicks on Pickup button
    And user select Date and tomorrows Time from Calendar
    And user clicks on Continue
    And On Menu Page click on Catering
    And On Menu Page click on Sweet-Tooth Sampler
    And user clicks pick for me  
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 18            |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
