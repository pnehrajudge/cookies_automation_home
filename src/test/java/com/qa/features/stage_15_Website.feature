

Feature: Order-Stage 15
  I want to use this template for placing orders

Background: user access to website
    Given User opens the Website
    
    # =========================DELIVERY ORDER====================================================================================
    
 
  Scenario: TC001 - Verify user is able to place order using credit card for store : 135 South Broad St, Phil PA
    Given user clicks on Order button
    When user enters the address
      | address | 135 South Broad St, Phil PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
 

  Scenario: TC002 - Verify user is able to place order using credit card for store : 620 Main St, Blacksburg VA
    Given user clicks on Order button
    When user enters the address
      | address | 620 Main St, Blacksburg VA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
    Scenario: TC003 - Verify user is able to place order using credit card for store : 1084 Lancaster Ave, Bryn Mawr
    Given user clicks on Order button
    When user enters the address
      | address | 1084 Lancaster Ave, Bryn Mawr |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
      |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
    Scenario: TC004 - Verify user is able to place order using credit card for store : 237 E 53rd St, New York NY
    Given user clicks on Order button
    When user enters the address
      | address | 237 E 53rd St, New York NY |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
    Scenario: TC005 - Verify user is able to place order using credit card for store : 3400 Lancaster Ave, Phil PA
     Given user clicks on Order button
    When user enters the address
      | address | 3400 Lancaster Ave, Phil PA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
     Scenario: TC006 - Verify user is able to place order using credit card for store : 30 Main St, Amherst MA
    Given user clicks on Order button
    When user enters the address
      | address | 30 Main St, Amherst MA |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
 
 Scenario: TC007 - Verify user is able to place order using credit card for store : 3301 N Charles St, Baltimore MD
    Given user clicks on Order button
    When user enters the address
      | address | 3301 N Charles St, Baltimore MD |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    Scenario: TC008 - Verify user is able to place order using credit card for store : 305 main St, Chico CA
     Given user clicks on Order button
    When user enters the address
      | address | 305 main St, Chico CA  |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
 
 
  Scenario: TC009 - Verify user is able to place order using credit card for store : 812 S Broadway Baltimore MD
     Given user clicks on Order button
    When user enters the address
      | address | 812 S Broadway Baltimore MD  |
    And user clicks on Delivery button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Insomniac
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under Delivery Info & your Info
      | recepient name   | Theresa Ainsworth |
      | recepient phone  |        2345678981 |
      | customer name    | Shaurya Nigam     |
      | customer phone   |        3457689024 |
      | customer emailID | snigam@judge.com  |
    And user enter delivery message
      | instructions | deliver fresh warm cookies |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    #==========================GEAR ORDER============================================================================================
    
     Scenario: Verify user is able to order gears using credit card
    When user clicks on Gifts tab
    And user clicks Gear option
    And user selects moon tee
    And user clicks add product button
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Shaurya Nigam     |
      | customer phone     |        3457689024 |
      | customer emailID   | snigam@judge.com  |
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
      |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
    # =========================SHIP COOKIES ORDER====================================================================================
    
    @stage15
    Scenario: TC010 - Verify user is able to place Ship Cookies Order using credit card
    When user clicks on Gifts tab
    And user clicks Ship cookies option
    And user selects Twelve cookies Gift box
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters Valid Details under recipient Delivery Info & your Info
      | recepient name     | Theresa Ainsworth |
      | recepient phone    |        2345678981 |
      | customer full name | Shaurya Nigam     |
      | customer phone     |        3457689024 |
      | customer emailID   | snigam@judge.com  |
    And user enters shipping address
      | Delivery Address | 1084 lancaster avenue bryn mawr |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
   # =========================PICKUP ORDER==========================================================================================
    
    @stage15_test
     Scenario: TC012 - Verify user is able to place order using credit card for store : 1105 E 55th St, Chicago, IL
      Given user clicks on Order button
    When user enters the address
      | address | 1105 E 55th St, Chicago, IL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    
    Scenario: TC013 - Verify user is able to place order using credit card for store : 2121 Cumberland Ave, Knoxville TN
    Given user clicks on Order button
    When user enters the address
      | address | 2121 Cumberland Ave, Knoxville TN |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19           |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
   
   
   Scenario: TC014 - Verify user is able to place order using credit card for store : 4319 Main St, Phil PA
   Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St, Phil PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    Scenario: TC015 - Verify user is able to place order using credit card for store : 733 West Cross St, Ypsilanti MI
 Given user clicks on Order button
    When user enters the address
      | address | 4319 Main St, Phil PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19           |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    Scenario: TC016 - Verify user is able to place order using credit card for store : 1537 N Milwaukee Ave, Chicago IL
    Given user clicks on Order button
    When user enters the address
      | address | 1537 N Milwaukee Ave, Chicago IL |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    Scenario: TC017 - Verify user is able to place order using credit card for store : 421 E Beaver Ave, State College  PA
     Given user clicks on Order button
    When user enters the address
      | address | 421 E Beaver Ave, State College  PA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    Scenario: TC018 - Verify user is able to place order using credit card for store : 128 W Chimes St, Baton Rouge LA
     Given user clicks on Order button
    When user enters the address
      | address | 128 W Chimes St, Baton Rouge LA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    Scenario: TC019 - Verify user is able to place order using credit card for store : 700 Cleburne Terrace, Atlanta GA
     Given user clicks on Order button
    When user enters the address
      | address | 700 Cleburne Terrace, Atlanta GA |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    Scenario: TC020 - Verify user is able to place order using credit card for store : 2302 Hillsborough St, Raleigh NC
     Given user clicks on Order button
    When user enters the address
      | address | 2302 Hillsborough St, Raleigh NC |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary
    
    Scenario: TC021 - Verify user is able to place order using credit card for store : 87 Mill St, Starkville MS 
    Given user clicks on Order button
    When user enters the address
      | address | 87 Mill St, Starkville MS  |
    And user clicks on Pickup button
    And user select Date and Time from Calendar
    And user clicks on Continue
    And On Menu Page click on The Major Rager
    And user clicks on add product
    And user goes to the cart
    And user clicks on the checkout button
    And user enters into your Info
      | customer name    | Shaurya Nigam    |
      | customer phone   |       3457689024 |
      | customer emailID | snigam@judge.com |
    And user selects Payment Method as Credit Card
    And user enters credit card details
      | cc no     | 4111111111111111 |
      | cc expiry | 12 19            |
       |cvv| 111 |
    And user clicks on Place Order
    Then Order confirmation page should appear displaying Order summary 
    